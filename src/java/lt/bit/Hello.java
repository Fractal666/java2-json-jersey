/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Fractal
 */
@Path("/hello")

public class Hello {
    
    private static final Log log = LogFactory.getLog(Hello.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List sayHello() {
        EntityManager em = EMF.getEntityManager();
        Query query = em.createQuery("Select p from People p");
        List peopleList = query.getResultList();
        EMF.returnEntityManager(em);
        return peopleList;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/(id)")
    public People getPerson(@QueryParam("d") String d, @PathParam("id") Integer id, @QueryParam("q") String q) {
        EntityManager em = EMF.getEntityManager();
        People p = em.find(People.class, id);
        EMF.returnEntityManager(em);
        return p;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public People dontSayNothing(People p) throws Exception {
        log.info("we are here: " + p);
        EntityManager manager = EMF.getEntityManager();
        EntityTransaction transaction = EMF.getTransaction(manager);
        if (p.getId() == null) {

            manager.persist(p);
        }
        EMF.commitTransaction(transaction);
        EMF.returnEntityManager(manager);

        return p;
    }
}
